<?php

namespace frontend\controllers;

use common\models\Account;
use common\models\User;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class AccountController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $user = User::findIdentity(\Yii::$app->user->getId());

//        if (!$user = User::findIdentity(\Yii::$app->user->getId())) {
//            \Yii::$app->session->setFlash('error','You are not authorized!');
//            return $this->goHome();
//        }

        if (!$model = Account::findOne(['user_id' => $user->id])) {
            throw new NotFoundHttpException('Account doesn\'t exist!');
        }

        return $this->render('index',[
            'user' => $user,
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success','Name was successfully updated.');
            return $this->redirect(['/account']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Account
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Account::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
