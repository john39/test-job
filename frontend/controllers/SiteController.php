<?php
namespace frontend\controllers;

use common\models\User;
use Yii;
use yii\base\Exception;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\SignupForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/account');
        }
        return $this->render('index');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/account');
        }

        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            try {
                $model->sendActivationLink();
                Yii::$app->session->setFlash('success','Please check your email for further instructions.');
                return $this->goHome();
            } catch (Exception $exception) {
                Yii::$app->session->setFlash('error',$exception->getMessage());
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * @param $token
     * @return string|\yii\web\Response
     */
    public function actionActivateEmail($token)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (!$model = User::findByActivationToken($token)) {
            Yii::$app->session->setFlash('error','Invalid URL or URL was already activated!');
            return $this->goHome();
        }

        switch ($model->status) {
            case User::STATUS_WAIT_ACTIVATION:
                $model->trigger(User::EVENT_ACTIVATED);
                break;
            case User::STATUS_DELETED:
                Yii::$app->session->setFlash('error','Your account has been blocked!');
                return $this->goHome();
                break;
        }

        Yii::$app->user->login($model);

        return $this->redirect('/account');
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
