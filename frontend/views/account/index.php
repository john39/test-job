<?php
/* @var $this yii\web\View */
use common\models\Account;
use common\models\User;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $model Account
 * @var $user User
 */

$this->title = 'Account';
?>

<?= Alert::widget()?>

<div class="account">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= Alert::widget()?>

    <?= $model->name ? Html::tag('h3','Welcome, '.$model->name) : '';?>

    <hr>

    <div class="row">
        <div class="col-xs-6">
            <p>Your current balance:</p>
            <pre><?= $model->showBalance();?></pre>

            <p>Your personal API key:</p>
            <pre><?= $user->api_key;?></pre>
        </div>
    </div>

    <hr>

    <p>Update your name:</p>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'form-username',
                'action' => Url::toRoute(['account/update','id' => $model->id])
            ]); ?>

            <?= $form->field($model, 'name')->textInput(['placeholder' => 'Ivan Ivanov'])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Update name', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>