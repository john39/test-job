<?php
namespace frontend\models;

use yii\base\Exception;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
        ];
    }

    /**
     * Send activation link
     *
     * @return User|null
     * @throws Exception
     */
    public function sendActivationLink()
    {
        if (!$this->validate()) {
            throw new Exception('Validation errors!');
        }

        // Find user or create new
        if (!$user = User::findOne(['email' => $this->email])) {
            $user = new User([
                'email' => $this->email,
                'status' => User::STATUS_WAIT_ACTIVATION,
            ]);
        }

        // Restrict multiple email sending if activate url was already sent(auth_key generated)
        if ($user->isAuthKeyExist()) {
            throw new Exception('Activation link was already sent! Please check your E-mail.');
        }

        // SignUp new user
        $user->generateAuthKey();
        if (!$user->save()) {
            throw new Exception('Internal server errors! Please try again later or contact us.');
        }
        $user->trigger(User::EVENT_SIGNUP);

        return $user;
    }
}
