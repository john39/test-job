<?php
namespace frontend\tests\unit\models;

use common\fixtures\UserFixture;
use common\models\User;
use frontend\models\SignupForm;

class SignupFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;


    public function _before()
    {
        $this->tester->haveFixtures([
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);
    }

    public function testValidation()
    {
        $model = new SignupForm();

        $model->email = null;
        $this->assertFalse($model->validate(['email']));

        $model->email = 'notemail';
        $this->assertFalse($model->validate(['email']));
    }

    public function isActivationEmailSent($user)
    {
        // Check activated email
        $this->tester->seeEmailIsSent();
        $emailMessage = $this->tester->grabLastSentEmail();
        expect('valid email is sent', $emailMessage)->isInstanceOf('yii\mail\MessageInterface');
        expect($emailMessage->getTo())->hasKey($user->email);
        expect($emailMessage->getFrom())->hasKey(\Yii::$app->params['adminEmail']);
        expect($emailMessage->getSubject())->equals('Activation link');
        expect($emailMessage->toString())->contains('Follow the link below');
        expect($emailMessage->toString())->contains($user->auth_key);
    }

    public function testSignup()
    {
        $model = new SignupForm([
            'email' => 'some_email@example.com',
        ]);
        $user = $model->sendActivationLink();

        // Check new model
        expect($user)->isInstanceOf('common\models\User');
        expect($user->email)->equals('some_email@example.com');
        expect($user->auth_key)->notNull();
        expect($user->status)->equals(User::STATUS_WAIT_ACTIVATION);

        $this->isActivationEmailSent($user);
    }

    public function testLogin()
    {
        $model = new SignupForm([
            'email' => 'activated@jenkins.info',
        ]);

        $user = $model->sendActivationLink();

        expect($user)->isInstanceOf('common\models\User');
        expect_not($user->isNewRecord);
        expect($user->email)->equals('activated@jenkins.info');

        $this->isActivationEmailSent($user);
    }

    public function testRepeatSignup()
    {
        $model = new SignupForm([
            'email' => 'emailsent@jenkins.info',
        ]);
        $this->tester->expectException('Exception', function() use ($model) {
            $model->sendActivationLink();
        });
        $this->tester->dontSeeEmailIsSent();
    }
}
