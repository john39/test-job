<?php
use common\fixtures\AccountFixture;
use common\fixtures\UserFixture;

class AccountCest
{
    public $apiKey = '?api_key=RkD_Jw0_8HEedzLk7MM-ZKEFfYR7VbMr';
    public $wrongApiKey = '?api_key=123';
    public $blockedAccountApiKey = '?api_key=OMEKCqtDQOdQ2OWpgiKRWYyzznsd';

    public function _before(ApiTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'account' => [
                'class' => AccountFixture::class,
                'dataFile' => codecept_data_dir() . 'account.php'
            ],
        ]);
    }

    public function access(ApiTester $I)
    {
        $I->sendGET('/account/view');
        $I->seeResponseCodeIs(401);
        $I->seeResponseIsJson();

        $I->sendGET('/account/view'.$this->wrongApiKey);
        $I->seeResponseCodeIs(401);
        $I->seeResponseIsJson();
    }

    public function badMethod(ApiTester $I)
    {
        $I->sendPOST('/account/view'.$this->apiKey);
        $I->seeResponseCodeIs(405);
        $I->seeResponseIsJson();

        $I->sendGET('/account/withdraw'.$this->apiKey);
        $I->seeResponseCodeIs(405);
        $I->seeResponseIsJson();
    }

    public function blocked(ApiTester $I)
    {
        $I->sendGET('/account/view'.$this->blockedAccountApiKey);
        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            'message' => 'Your account has been blocked!',
        ]);
    }

    public function viewOwnAccount(ApiTester $I)
    {
        $I->sendGET('/account/view'.$this->apiKey);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'name' => 'John Vavilin',
            'balance' => 1000,
        ]);
    }

    public function withdrawSuccess(ApiTester $I)
    {
        $I->sendPOST('/account/withdraw'.$this->apiKey, [
            'amount' => 550,
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'message' => 'Funds were successfully withdrawn.',
            'balance' => '450.00'
        ]);
    }

    public function withdrawEmpty(ApiTester $I)
    {
        // Negative
        $I->sendPOST('/account/withdraw'.$this->apiKey);
        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            'message' => 'Amount cannot be blank.',
        ]);
    }

    public function withdrawNotNumber(ApiTester $I)
    {
        // Not number
        $I->sendPOST('/account/withdraw'.$this->apiKey, [
            'amount' => 'notnumber',
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            'message' => 'Amount must be a number.',
        ]);
    }

    public function withdrawNegative(ApiTester $I)
    {
        // Negative
        $I->sendPOST('/account/withdraw'.$this->apiKey, [
            'amount' => -100,
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            'message' => 'Amount must be no less than 0.',
        ]);
    }

    public function withdrawNoMoney(ApiTester $I)
    {
        // No money
        $I->sendPOST('/account/withdraw'.$this->apiKey, [
            'amount' => 10000,
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            'message' => 'Not enough money on balance!',
        ]);
    }
}