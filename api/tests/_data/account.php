<?php
return [
    [
        'id' => 1,
        'user_id' => 1,
        'name' => 'John Vavilin',
        'balance' => 1000,
        'status' => 1
    ],
    [
        'id' => 2,
        'user_id' => 2,
        'name' => 'John Nozhd',
        'balance' => 500,
        'status' => 0
    ],
];