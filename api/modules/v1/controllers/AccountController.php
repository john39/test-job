<?php
namespace api\modules\v1\controllers;

use api\modules\v1\components\ApiController;
use common\helpers\ModelErrorHelper;
use common\models\User;
use yii\base\DynamicModel;
use yii\web\HttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class AccountController extends ApiController
{
    public $modelClass = 'common\models\Account';

    public function actions()
    {
        return [];
    }

    public function actionView()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if (!$user = User::findIdentityByAccessToken(\Yii::$app->request->get('api_key'))) {
            throw new UnauthorizedHttpException();
        }

        $account = $user->getAccountDetails();

        return [
            'name' => $account->name,
            'balance' => $account->balance
        ];
    }

    public function actionWithdraw()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        if (!\Yii::$app->request->isPost) {
            throw new MethodNotAllowedHttpException();
        }
        $post = \Yii::$app->request->post();
        $amount = $post['amount'] ?? null;

        // Do validation
        $model = DynamicModel::validateData(compact('amount'),[
            ['amount','required'],
            ['amount','number','min' => 0]
        ]);
        if ($model->hasErrors()) {
            throw new HttpException(422,ModelErrorHelper::getErrorsString($model));
        }

        // Find user and his account
        if (!$user = User::findIdentityByAccessToken(\Yii::$app->request->get('api_key'))) {
            throw new UnauthorizedHttpException();
        }
        $account = $user->getAccountDetails();

        // Update account balance
        $account->balance -= abs($post['amount']);
        if ($account->balance < 0) {
            throw new HttpException(422,'Not enough money on balance!');
        }
        if (!$account->save()) {
            throw new HttpException(500);
        }

        return [
            'balance' => $account->showBalance(false),
            'message' => 'Funds were successfully withdrawn.',
            'status' => 200
        ];
    }
}