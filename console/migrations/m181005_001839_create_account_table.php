<?php

use yii\db\Migration;

/**
 * Handles the creation of table `account`.
 */
class m181005_001839_create_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('account', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unique(),
            'name' => $this->string(150)->defaultValue(null),
            'balance' => $this->float()->defaultValue(0),
            'status' => $this->integer()->defaultValue(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('account');
    }
}
