<?php

namespace common\helpers;
use yii\helpers\Html;

/**
 * Class ModelErrorHelper
 * @package backend\helpers
 */
class ModelErrorHelper
{
    /**
     * @param $model
     * @return string
     */
    public static function getErrorsString($model)
    {
        $resultString = '';
        foreach ($model->getErrors() as $error) {
            $resultString .= $error[0];
        }
        return $resultString;
    }
}