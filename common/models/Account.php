<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "account".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property double $balance
 * @property int $status
 */
class Account extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const BONUS_BALANCE = 1000;
    const ROUBLE_SYMBOL = 'P';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'unique'],
            [['user_id', 'status'], 'integer'],
            [['balance'], 'number'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'balance' => 'Balance',
            'status' => 'Status',
        ];
    }

    /**
     * @return string
     */
    public function showBalance($showCurrency = true)
    {
        $balance = number_format($this->balance,2,'.','');
        return $showCurrency ? $balance . self::ROUBLE_SYMBOL : $balance;
    }
}
