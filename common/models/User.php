<?php
namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\web\IdentityInterface;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $api_key
 * @property string $password write-only password
 *
 * @property Account $account
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_WAIT_ACTIVATION = 20;

    const EVENT_SIGNUP = 'signup';
    const EVENT_ACTIVATED = 'activated';

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_SIGNUP,[$this,'signupCallback']);
        $this->on(self::EVENT_ACTIVATED,[$this,'activatedCallback']);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::class,['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_WAIT_ACTIVATION],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED,self::STATUS_WAIT_ACTIVATION]],
        ];
    }

    /**
     * @param $event
     * @return bool
     * @throws \yii\base\Exception
     */
    public function signupCallback($event)
    {
        $isMailSent = Yii::$app->mailer->compose('activationLink',['model' => $this])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($this->email)
            ->setSubject('Activation link')
            ->send();
        if (!$isMailSent) {
            throw new \yii\base\Exception('Error while sending activation E-mail! Please try again later or contact us.');
        }
        return true;
    }

    /**
     * @param $event
     * @throws Exception
     */
    public function activatedCallback($event)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            // Update user details
            $this->generateApiKey();
            $this->status = self::STATUS_ACTIVE;
            $this->auth_key = null;
            if (!$this->save()) {
                throw new Exception('Error while registering! Please try again later or contact us.');
            }

            // Create new Account model assoc. with current user
            $account = new Account([
                'user_id' => $this->id,
                'balance' => Account::BONUS_BALANCE,
                'status' => Account::STATUS_ACTIVE
            ]);
            if (!$account->save()) {
                throw new Exception('Error while creating account! Please try again later or contact us.');
            }

            $transaction->commit();
            Yii::$app->session->setFlash('success','Welcome! You have got a bonus - ' . $account->showBalance());
        } catch (Exception $exception) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error',$exception->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['api_key' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @param $token
     * @return null|static
     */
    public static function findByActivationToken($token)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @return bool
     */
    public function isWaitingActivation()
    {
        return $this->status == self::STATUS_WAIT_ACTIVATION;
    }

    /**
     * @return bool
     */
    public function isAuthKeyExist()
    {
        return $this->auth_key ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generate API key
     */
    public function generateApiKey()
    {
        $this->api_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return Account
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function getAccountDetails()
    {
        if (!$this->account) {
            throw new NotFoundHttpException();
        }
        if ($this->account->status == Account::STATUS_INACTIVE) {
            throw new HttpException(422,'Your account has been blocked!');
        }

        return $this->account;
    }
}
