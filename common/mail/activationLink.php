<?php
use common\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model User */
$activateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate-email', 'token' => $model->auth_key]);
?>
<div class="password-reset">
    <p>Hello,</p>
    <p>Follow the link below to Sign-Up/Sign-In:</p>
    <p><?= Html::a(Html::encode($activateLink), $activateLink) ?></p>
</div>
